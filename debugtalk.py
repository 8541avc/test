
import time

def sleep(n_secs):
    time.sleep(n_secs)

sso_special_cookie='tif_ssoid=a5d57a56e34a59f9678f73a66267e3fa;acw_tc=2f6a1fd815876435293077872e8baca57f879abde1bd8f106dea18e9a50706'
def get_sso_special_cookie():
    return sso_special_cookie

#个人登录：

sso_hum_cookie='tif_ssoid=e699dc256f5d0b2cb1a8a86e0304bcb0'
def get_sso_hum_cookie():
    return sso_hum_cookie

#企业账号登录：

sso_corp_cookie='tif_ssoid=e699dc256f5d0b2cb1a8a86e0304bcb0'
def get_sso_corp_cookie():
    return sso_corp_cookie

base_url = "http://bs-test.digitalgd.com.cn"

def get_base_url():
    return base_url