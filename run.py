import smtplib, time, os
import base64
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.header import Header
from httprunner.api import HttpRunner
from httprunner.report import gen_html_report

def send_mail_html(file):
    # 配置邮件发送者及接收对象
    mail_pass = 'XayDD8X8dw7gc8sY'
    mail_host = "smtp.exmail.qq.com"  # 设置服务器
    mail_user = "v_grchen@digitalgd.com.cn"  # 用户名，需要自行修改
    sender = 'v_grchen@digitalgd.com.cn' # 用户名，需要自行修改
    # receivers = ['beeliao@digitalgd.com.cn']  # 接收邮件，可设置为你的QQ邮箱或者其他邮箱
    receivers = ['865344941@qq.com']

    # 读取html文件内容
    f = open(file, 'rb')
    mail_body = f.read()
    f.close()

    # 创建一个带附件的实例
    message = MIMEMultipart()
    message['From'] = Header("粤商通接口自动化测试", 'utf-8')
    message['To'] = Header("粤商通项目组", 'utf-8')
    t = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())
    subject = '粤商通接口自动化测试报告' + t
    message['Subject'] = Header(subject, 'utf-8')

    # 邮件正文内容
    message.attach(MIMEText(mail_body, _subtype='html', _charset='utf-8'))
    # 添加附件
    att = MIMEText(mail_body,  "base64", "utf-8")
    att["Content-Type"] = 'application/octet-stream'
    times = time.strftime("%m_%d_%H_%M", time.localtime(time.time()))
    filename_report = 'yst_Api_Report' + '_' + times + '.html'
    att["Content-Disposition"] = 'attachment; filename= %s ' %filename_report
    message.attach(att)

    # 登录并发送邮件
    try:
        smtp = smtplib.SMTP()
        smtp.connect(mail_host, 25)  # 25 为 SMTP 端口号
        smtp.login(mail_user, mail_pass)
        smtp.sendmail(sender, receivers, message.as_string())
    except:
        print("邮件发送失败！")
    else:
        print("邮件发送成功！")
    finally:
        smtp.quit()


def find_new_file(dir):
    '''查找目录下最新的文件'''
    file_lists = os.listdir(dir)
    file_lists.sort(key=lambda fn: os.path.getmtime(dir + "\\" + fn)
                    if not os.path.isdir(dir + "\\" + fn)
                    else 0)
    # print('最新的文件为： ' + file_lists[-1])
    file = os.path.join(dir, file_lists[-1])
    print('完整文件路径：', file)
    return file


if __name__ == "__main__":
    # 1、创建HttpRunner对象
    # failfast当用例执行失败之后,会自动暂停,默认为False,可以不写
    runner = HttpRunner(failfast=False, log_level='INFO')

    # 2、运行用例
    # run方法支持如下参数:
    # yml用例文件的路径
    # 字典(用例的信息)
    runner.run('testsuites/')
    t = time.strftime("%Y-%m-%d", time.localtime())
    gen_html_report(runner._summary, report_dir='reports/')

    dir = 'reports/'  # 指定文件目录
    file = find_new_file(dir)  # 查找最新的html文件
    send_mail_html(file)  # 发送html内容邮件 1
